<?php


/** this function is the core of this module. It processes a bracket
  * notation and returns an img tag for the syntax tree
  */

function phpsyntaxtree_phpsyntaxtree($bracket_notation) {

  //we use here md5 to generate (hopefully) unique filenames
  $file_name = md5($bracket_notation) . '.png';
  $file_dir  = file_directory_path() . '/phpsyntaxtree/';
  $file_path = $file_dir . $file_name;

  if (!realpath($file_dir)) { mkdir(realpath(file_directory_path()) . '/phpsyntaxtreen'); }

  //if the file does not exists we create a new image.
  if (!realpath($file_path)) {
    phpsyntaxtree_generate_image($bracket_notation, realpath($file_dir) . '/' . $file_name);
  }

  return theme('image', $file_path, $bracket_notation);
}

function phpsyntaxtree_generate_image($bracket_notation, $filename) {
// exit($filename);
  $module_path = drupal_get_path('module', 'phpsyntaxtree');
  set_include_path(get_include_path() . PATH_SEPARATOR . $module_path . '/phpsyntaxtree/');

  require_once 'phpsyntaxtree/src/class.elementlist.php';
  require_once 'phpsyntaxtree/src/class.stringparser.php';
  require_once 'phpsyntaxtree/src/class.treegraph.php';

  $color     = variable_get('phpsyntaxtree_color', 0);
  $triangles = variable_get('phpsyntaxtree_triangles', TRUE);
  $antialias = variable_get('phpsyntaxtree_antialias', 0);
  $autosub   = variable_get('phpsyntaxtree_autosub', 0);
  $font      = variable_get('phpsyntaxtree_font', 'Vera.ttf');
  $fontsize  = variable_get('phpsyntaxtree_fontsize', 8);

  //the code below is copied from phpSyntaxTree's stgraph.png
  //we can't use the file by itself because it assumes that we're using session
  //which we're not.
  $sp = new CStringParser($bracket_notation);

  if (!$sp->Validate() )
  {
    // Display an error if the phrase doesn't validate.
    //   Right now that only means the brackets didn't
    //   match up. More tests could be added in the future.

    // It would also be a good idea to make this error
    //   image creation a standalone class or something.

    $im = imagecreate( 350, 20 );
    $col_bg = imagecolorallocate ($im, 255, 255, 255);
    $col_fg = imagecolorallocate ($im, 255, 0, 0);

    imagestring( $im, 3, 5, 3, "Sentence brackets don't match up...", $col_fg );
    imagepng($im, $filename);
  } else {
      // If all is well, go ahead and draw the graph ...

      $sp->Parse();

      if ( $autosub )
          $sp->AutoSubscript();

      $elist = $sp->GetElementList();

      $font_path = realpath($module_path . '/phpsyntaxtree/ttf/' . $font);

      // Draw the graph
      $graph = new CTreegraph($elist, $color, $antialias, $triangles, $font_path, $fontsize );
      $graph->Save($filename);
  }
}
